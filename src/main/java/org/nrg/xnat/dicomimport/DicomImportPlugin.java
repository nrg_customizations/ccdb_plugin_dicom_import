package org.nrg.xnat.dicomimport;

import org.nrg.framework.annotations.XnatPlugin;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.ImportResource;

@XnatPlugin(value = "ccdb_plugin_dicom_import", name = "XNAT 1.7 Dicom Import Plugin", description = "This is the XNAT 1.7 Dicom Import Plugin.")
@ImportResource("WEB-INF/conf/dicom-import-context.xml")
public class DicomImportPlugin {
}
